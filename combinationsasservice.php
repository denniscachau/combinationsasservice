<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Combinationsasservice extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'combinationsasservice';
        $this->tab = 'merchandizing';
        $this->version = '1.1.0';
        $this->author = 'Ciren';
        $this->need_instance = 0;
        $this->module_key = '51f27b8dc5076e80f71b8b84b12ce40d';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Use combinations as service');
        $this->description = $this->l('Use combinations as service');

        $this->ps_versions_compliancy = array('min' => '1.7.2.0', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        Configuration::updateValue('PS_DISPLAY_QTIES', 0);

        return parent::install() &&
            $this->addTab() &&
            $this->registerHook('displayBackOfficeHeader') &&
            $this->registerHook('displayAdminProductsCombinationBottom') &&
            $this->registerHook('actionAdminProductsListingResultsModifier') &&
            $this->registerHook('actionProductSave') &&
            $this->registerHook('actionValidateOrder');
    }

    protected function addTab()
    {
        $tab = new Tab();
        $tab->class_name = 'AdminCombinationsAsService';
        $tab->id_parent = 0;
        $tab->module = $this->name;
        $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->displayName;
        $tab->active = 0;
        $tab->add();

        return true;
    }

    public function uninstall()
    {
        return parent::uninstall();
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (!Module::isEnabled($this->name)) {
            return '';
        }

        $js_path = '<script>
            var admin_combinationsasservice_ajax_url = ' . (string) json_encode(
            pSQL($this->context->link->getAdminLink('AdminCombinationsAsService'))
        ) . ';</script>';

        return $js_path . $this->context->smarty->fetch(
            $this->getLocalPath() . 'views/templates/admin/display_back_office_header.tpl'
        );
    }

    public function hookDisplayAdminProductsCombinationBottom($params)
    {
        $is_service = (bool)Db::getInstance()->getValue(
            "SELECT *
            FROM `" . _DB_PREFIX_ . $this->name . "`
            WHERE `id_product` = '" . (int)$params['id_product'] . "'
            AND `id_product_attribute` = '" . (int)$params['id_product_attribute'] . "'"
        );

        $this->context->smarty->assign(array(
            'is_service' => $is_service,
            'id_product_attribute' => $params['id_product_attribute']
        ));

        return $this->context->smarty->fetch(
            $this->local_path . 'views/templates/admin/configure.tpl'
        );
    }

    public function hookActionProductSave($params)
    {
        $id_product = (int)$params['id_product'];
        $product_attributes = Db::getInstance()->executeS(
            "SELECT `id_product_attribute`
            FROM `" . _DB_PREFIX_ . "product_attribute`
            WHERE `id_product` = '" . (int)$id_product . "'"
        );

        Db::getInstance()->delete($this->name, 'id_product = ' . (int)$id_product);

        $queries = array();

        foreach ($product_attributes as $product_attribute) {
            $id_product_attribute = (int) $product_attribute['id_product_attribute'];
            if (Tools::getValue('CAA_service_' . $id_product_attribute)) {
                $queries[] = array(
                    'id_product' => $id_product,
                    'id_product_attribute' => $id_product_attribute
                );
            }
        }

        foreach ($queries as $query) {
            if (Db::getInstance()->insert($this->name, $query) == false) {
                return false;
            }
        }

        return true;
    }

    public function hookActionValidateOrder($params)
    {
        $order = $params['order'];

        $this->refreshQuantities($order);
    }

    public function hookActionOrderStatusPostUpdate($params)
    {
        $id_order = (int)$params['id_order'];
        $order = new Order($id_order);

        $this->refreshQuantities($order);
    }

    /**
     * @param Order $order
     */
    private function refreshQuantities($order)
    {
        foreach ($order->getProductsDetail() as $product) {
            $is_service = (bool)Db::getInstance()->getValue(
                "SELECT `id_product`
                FROM `" . _DB_PREFIX_ . $this->name . "`
                WHERE `id_product` = '" . (int)$product['product_id'] . "'
                AND `id_product_attribute` = '" . (int)$product['product_attribute_id'] . "'"
            );

            if ($is_service) {
                StockAvailable::setQuantity((int)$product['product_id'], (int)$product['product_attribute_id'], 9999);
            }
        }
    }

    public function hookActionAdminProductsListingResultsModifier($params)
    {
        if (!array_key_exists('sav_quantity', $params['products'][0])) {
            return true;
        }

        foreach ($params['products'] as &$product) {
            $count_service = (int)Db::getInstance()->getValue(
                "SELECT COUNT(*)
                FROM `" . _DB_PREFIX_ . $this->name . "`
                WHERE `id_product` = '" . (int)$product['id_product'] . "'
                GROUP BY `id_product`"
            );

            $product['sav_quantity'] -= 9999 * $count_service;
        }
    }
}
