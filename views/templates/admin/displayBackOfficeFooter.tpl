{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2020 PrestaShop SA
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
  $('document').ready(function() {
    let quantity

    $('#form').on('change', '.CAA_service', function() {
      let id_product_attribute = $(this).next().val();
      if ($(this).prop("checked")) {
        quantity = $('#combination_' + id_product_attribute + '_attribute_quantity').val();
        $('#combination_' + id_product_attribute + '_attribute_quantity').val('9999');
        $('#combination_' + id_product_attribute + '_attribute_quantity').attr('readonly', '');
        $('#attribute_' + id_product_attribute + ' .attribute-quantity input').val('9999');
        $('#attribute_' + id_product_attribute + ' .attribute-quantity input').attr('readonly', '');
      } else {
        $('#combination_' + id_product_attribute + '_attribute_quantity').val(quantity);
        $('#combination_' + id_product_attribute + '_attribute_quantity').removeAttr('readonly');
        $('#attribute_' + id_product_attribute + ' .attribute-quantity input').val(quantity);
        $('#attribute_' + id_product_attribute + ' .attribute-quantity input').removeAttr('readonly');
      }
    })
  })
</script>
