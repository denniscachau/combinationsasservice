{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2020 PrestaShop SA
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<h2 class="title">{l s='Use this combination as a service' mod='combinationsasservice'}</h2>
<div class="checkbox">
  <label>
    <input type="checkbox" id="CAA_service_{$id_product_attribute|intval}" name="CAA_service_{$id_product_attribute|intval}" class="attribute_default_checkbox CAA_service" value="1" {if $is_service}checked="checked"{/if}>
    <input type="hidden" value="{$id_product_attribute|intval}">
    {l s='Is this a service?' mod='combinationsasservice'}
  </label>
</div>

<script type="text/javascript">
  {if $is_service}
  $('document').ready(function() {
    $('#combination_{$id_product_attribute|intval}_attribute_quantity').attr('readonly', '');
    $('#attribute_{$id_product_attribute|intval} .attribute-quantity input').attr('readonly', '');
  })
  {/if}
</script>
