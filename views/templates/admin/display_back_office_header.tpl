{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2020 PrestaShop SA
* @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}

<script type="text/javascript">
    $(document).ready(function () {

        let form_id_product = $('#form_id_product');
        if (form_id_product.length <= 0) {
            return '';
        }

        let id_product = form_id_product.val();

        $.ajax({
            type: 'POST',
            url: admin_combinationsasservice_ajax_url,
            dataType: 'json',
            data: {
                controller : 'AdminCombinationsAsService',
                action : 'getServices',
                ajax : true,
                id_product : id_product,
            },
            success: function(data) {
                if (data.success == '1') {
                    let services = data.services;

                    $.each(services, function(i, value) {
                        refreshQuantities($('#CAA_service_' + value.id_product_attribute), value.id_product_attribute);
                    })
                }
            }
        });

        $('#form').on('change', '.CAA_service', function() {
            let id_product_attribute = $(this).next().val();

            refreshQuantities($(this), id_product_attribute);
        })

        function refreshQuantities(element, id_product_attribute)
        {
            let combination_element = $('#combination_' + id_product_attribute + '_attribute_quantity');
            let attribute_element = $('#attribute_' + id_product_attribute + ' .attribute-quantity input');
            if (element.prop("checked")) {
                combination_element.val('9999');
                combination_element.attr('readonly', '');
                attribute_element.val('9999');
                attribute_element.attr('readonly', '');
            } else {
                combination_element.val(0);
                combination_element.removeAttr('readonly');
                attribute_element.val(0);
                attribute_element.removeAttr('readonly');
            }
        }
    });
</script>
